package br.com.virtualtechnology.cryptographylib;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by vinic on 07/11/2017.
 */

public class AES {

        private Cipher cipherC;
        private Cipher cipherD;

        public AES(String senha) {
            try {
                cipherC = Cipher.getInstance("AES");
                cipherD = Cipher.getInstance("AES");
                SecretKey key = key = new SecretKeySpec(Arrays.copyOf(senha.getBytes("ASCII"), senha.length()), "AES");
                cipherC.init(Cipher.ENCRYPT_MODE, key);
                cipherD.init(Cipher.DECRYPT_MODE,key);
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            } catch (NoSuchPaddingException ex) {
                ex.printStackTrace();
            } catch (InvalidKeyException ex) {
                ex.printStackTrace();
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        }

    /*public static CriptografiaArq getInstance(){
        if(criptografia == null)criptografia = new CriptografiaArq();
        return criptografia;
    }*/

        public byte [] criptografa(String valor){
            try {
                return cipherC.doFinal(valor.getBytes("UTF-8"));
            } catch (IllegalBlockSizeException ex) {
                ex.printStackTrace();
            } catch (BadPaddingException ex) {
                ex.printStackTrace();
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        public byte [] criptografa(byte [] valor){
            try {
                return cipherC.doFinal(valor);
            } catch (IllegalBlockSizeException ex) {
                ex.printStackTrace();
            } catch (BadPaddingException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        public String descriptografa(byte [] valor){
            try {
                return new String(cipherD.doFinal(valor));
            } catch (IllegalBlockSizeException ex) {
                ex.printStackTrace();
            } catch (BadPaddingException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        public byte [] descriptografaByte(byte [] valor){
            try {
                return cipherD.doFinal(valor);
            } catch (IllegalBlockSizeException ex) {
                ex.printStackTrace();
            } catch (BadPaddingException ex) {
                ex.printStackTrace();
            }
            return null;
        }
}
